package es.vidal;

import es.vidal.controlador.MenuController;
import es.vidal.modelo.dao.FileTareaDAO;

public class TestTareaMVC {

    public static void main( String[] args ) {
        FileTareaDAO fileTareaDAO = new FileTareaDAO();

        MenuController menuController = new MenuController(fileTareaDAO);

        menuController.mostrarMenu();
    }
}
