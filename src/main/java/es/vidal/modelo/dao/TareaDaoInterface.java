package es.vidal.modelo.dao;

import es.vidal.modelo.Tarea;

import java.time.LocalDate;
import java.util.ArrayList;

public interface TareaDaoInterface {

    ArrayList<Tarea> findAll();

    Tarea findById(int codigo);

    ArrayList<Tarea> findByDate(LocalDate fecha);

    void save(Tarea tarea);

}
