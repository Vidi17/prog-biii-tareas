package es.vidal.modelo.dao;

import es.vidal.modelo.Prioridad;
import es.vidal.modelo.Tarea;

import java.io.*;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;

public class FileTareaDAO implements TareaDaoInterface{

    File file = new File("src/main/resources/tareas.txt");

    @Override
    public ArrayList<Tarea> findAll(){
        ArrayList<Tarea> tareasList = new ArrayList<>();
        String line;
        try (FileReader fileReader = new FileReader(file);
             BufferedReader bufferedReader = new BufferedReader(fileReader)){
            do {
                line = bufferedReader.readLine();
                if (line != null){
                    Tarea tareaToAdd = createTarea(line);
                    tareasList.add(tareaToAdd);
                }
            }while (line != null);
        }catch (IOException exception){
            exception.getStackTrace();
        }
        return tareasList;
    }

    public Tarea createTarea(String line){
        String[] tarea = line.split(";");
        int codigo = Integer.parseInt(tarea[0]);
        Prioridad prioridad = establecerPrioridad(tarea[1].trim());
        String descripcion =  tarea[2];
        LocalDateTime fechaCreacion = LocalDateTime.parse(tarea[3]);
        LocalDateTime fechaEntrega = LocalDateTime.parse(tarea[4]);
        boolean cerrada = establecerEstado(tarea[5]);
        return new Tarea(codigo, descripcion, prioridad, fechaCreacion, fechaEntrega, cerrada);
    }

    private boolean establecerEstado(String estado){
        return (estado.equals("true"));
    }

    private Prioridad establecerPrioridad(String prioridad){
        return switch (prioridad){
            case "CRITICA" -> Prioridad.CRITICA;
            case "ALTA" -> Prioridad.ALTA;
            case "NORMAL" -> Prioridad.NORMAL;
            case "BAJA" -> Prioridad.BAJA;
            default -> null;
        };
    }

    @Override
    public Tarea findById(int codigo) {
        String line;
        try (FileReader fileReader = new FileReader(file);
             BufferedReader bufferedReader = new BufferedReader(fileReader)){
            do {
                line = bufferedReader.readLine();
                if (line != null){
                    Tarea tareaToAdd = createTarea(line);
                    if (tareaToAdd.getCodigo() == (codigo)){
                        return tareaToAdd;
                    }
                }
            }while (line != null);
        }catch (IOException exception){
            exception.getStackTrace();
        }
        return null;
    }

    @Override
    public ArrayList<Tarea> findByDate(LocalDate fecha) {
        ArrayList<Tarea> tareas = new ArrayList<>();
        String line;
        try (FileReader fileReader = new FileReader(file);
             BufferedReader bufferedReader = new BufferedReader(fileReader)){
            do {
                line = bufferedReader.readLine();
                if (line != null){
                    Tarea tareaToAdd = createTarea(line);
                    if (tareaToAdd.getFechaEntrega().getDayOfYear() == (fecha.getDayOfYear())){
                        tareas.add(tareaToAdd);
                    }
                }
            }while (line != null);
        }catch (IOException exception){
            exception.getStackTrace();
        }
        return tareas;
    }

    @Override
    public void save(Tarea tarea) {
        ArrayList<Tarea> tareasList = findAll();
        if (tareasList.contains(tarea)){
            update(tarea);
        }else {
            insert(tarea);
        }
    }

    private boolean insert(Tarea tarea) {
        try (FileWriter fileWriter = new FileWriter(file, true);
             BufferedWriter bufferedWriter = new BufferedWriter(fileWriter)){
            String tareaToAdd = mapTareaToRegister(tarea);
            bufferedWriter.write(tareaToAdd);
            bufferedWriter.newLine();
        }catch (IOException ex){
            ex.getStackTrace();
            return false;
        }
        return true;
    }

    public boolean delete(int codigo) {
        ArrayList<Tarea> tareasList = findAll();
        try (FileWriter fileWriter = new FileWriter(file);
             BufferedWriter bufferedWriter = new BufferedWriter(fileWriter)){
            for (Tarea tarea : tareasList) {
                if (tarea.getCodigo() != codigo) {
                    bufferedWriter.append(mapTareaToRegister(tarea));
                    bufferedWriter.newLine();
                }
            }
        }catch (IOException ex){
            ex.getStackTrace();
        }
        return true;
    }

    private String[] tareaAsList(String tarea){
        return tarea.split(";");
    }

    private String mapTareaToRegister(Tarea tarea){
        return tarea.getCodigo() +
                ";" +
                tarea.getPrioridad() +
                ";" +
                tarea.getDescripcion() +
                ";" +
                tarea.getFechaCreacion() +
                ";" +
                tarea.getFechaEntrega() +
                ";" +
                tarea.isCerrada();
    }

    private void update(Tarea tarea) {
        delete(tarea.getCodigo());
        insert(tarea);
    }
}
