package es.vidal.modelo.dao;

import es.vidal.modelo.Prioridad;
import es.vidal.modelo.Tarea;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;

public class InMemoryTareaDAO implements TareaDaoInterface {

    private ArrayList<Tarea> listado = new ArrayList<>();

    public InMemoryTareaDAO(){
        this.listado.add(new Tarea(1, "Estudiar Java", Prioridad.ALTA,
                LocalDateTime.of(2021, 11,11,0,0,0)));
    }

    @Override
    public ArrayList<Tarea> findAll() {
        return listado;
    }

    @Override
    public Tarea findById(int codigo) {
        for (Tarea tarea : listado) {
            if (tarea.getCodigo() == codigo) {
                return tarea;
            }
        }
        return null;
    }

    @Override
    public ArrayList<Tarea> findByDate(LocalDate fecha) {
        ArrayList<Tarea> tareas = new ArrayList<>();
        for (Tarea tarea : listado) {
            if (tarea.getFechaEntrega().getDayOfYear() == fecha.getDayOfYear()) {
                tareas.add(tarea);
            }
        }
        return tareas;
    }

    @Override
    public void save(Tarea tarea) {
        if (listado.contains(tarea)) {
            int posicionTarea = listado.indexOf(tarea);
            listado.set(posicionTarea, tarea);
        } else {
            listado.add(tarea);
        }
    }
}
