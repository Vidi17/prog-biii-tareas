package es.vidal.modelo;

import java.time.LocalDateTime;
import java.util.Objects;

public class Tarea {

    private int codigo;

    private Prioridad prioridad;

    private String descripcion;

    private LocalDateTime fechaCreacion;

    private LocalDateTime fechaEntrega;

    private boolean cerrada = false;

    public Tarea(int codigo, String descripcion, Prioridad prioridad, LocalDateTime
            fechaEntrega) {

        this.codigo = codigo;
        this.prioridad = prioridad;
        this.descripcion = descripcion;
        this.fechaCreacion = LocalDateTime.now();
        this.fechaEntrega = fechaEntrega;
    }

    public Tarea(int codigo, String descripcion, Prioridad prioridad, LocalDateTime fechaCreacion, LocalDateTime
            fechaEntrega, boolean cerrada) {

        this.codigo = codigo;
        this.prioridad = prioridad;
        this.descripcion = descripcion;
        this.fechaCreacion = fechaCreacion;
        this.fechaEntrega = fechaEntrega;
        this.cerrada = cerrada;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Tarea tarea = (Tarea) o;
        return codigo == tarea.codigo;
    }

    @Override
    public int hashCode() {
        return Objects.hash(codigo);
    }

    public int getCodigo() {
        return codigo;
    }

    public Prioridad getPrioridad() {
        return prioridad;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public LocalDateTime getFechaCreacion() {
        return fechaCreacion;
    }

    public LocalDateTime getFechaEntrega() {
        return fechaEntrega;
    }

    public boolean isCerrada() {
        return cerrada;
    }

    public String isCerradaToString(){
        if (cerrada){
            return "Cerrada";
        }else {
            return "Abierta";
        }
    }

    public void setCerrada(boolean cerrada) {
        this.cerrada = cerrada;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public void setFechaEntrega(LocalDateTime fechaEntrega) {
        this.fechaEntrega = fechaEntrega;
    }
}


