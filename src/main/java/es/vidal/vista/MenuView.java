package es.vidal.vista;

import java.util.Scanner;

public class MenuView {

    private final int LISTAR_TAREAS = 1;
    private final int ANYADIR_TAREA = 2;
    private final int CERRAR_TAREA = 3;
    private final int CERRAR_TAREAS_DIA = 4;
    private final int MODIFICAR_TAREA = 5;

    private Scanner teclado = new Scanner(System.in);

    public int seleccionarOpcion(){
        System.out.printf("""
                Introduce una opción:
                %d - Listar todas las tareas
                %d - Añadir tarea
                %d - Cerrar tarea
                %d - Cerrar tareas de un día
                %d - Modificar tarea
                """, LISTAR_TAREAS, ANYADIR_TAREA, CERRAR_TAREA, CERRAR_TAREAS_DIA, MODIFICAR_TAREA);
        int opcion = teclado.nextInt();
        teclado.nextLine();

        return opcion;

    }
}
