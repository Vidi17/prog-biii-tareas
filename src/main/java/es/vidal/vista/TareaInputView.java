package es.vidal.vista;

import es.vidal.modelo.Prioridad;
import es.vidal.modelo.Tarea;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Scanner;

public class TareaInputView {

    private Scanner teclado;

    public TareaInputView() {
        this.teclado = new Scanner(System.in);
    }

    public Tarea getNuevaTarea(int codigo) {
        System.out.println("Vamos a introducir una nueva tarea");
        System.out.println("Introduzca la Descripción:");
        String descripcion = teclado.nextLine();
        System.out.println("Introduzca la Prioridad (Critica, Alta, Normal, Baja):");
        String prioridadString = teclado.nextLine();
        Prioridad prioridad = getPrioridad(prioridadString);
        System.out.println("Introduzca la fecha de entrega (dd-mm-yyyy):");
        DateTimeFormatter dateTimeFormatter =
                DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm:ss");
        LocalDateTime fechaEntrega =
                LocalDateTime.parse(teclado.nextLine() + " 00:00:00", dateTimeFormatter);
        return new Tarea(codigo, descripcion, prioridad, fechaEntrega);
    }

    public boolean getModificarDescripcion(){
        System.out.println("¿Quiere modificar la descripción?(Si / No)");
        String decision = teclado.nextLine();
        return decision.equals("Si");
    }

    public String getDescripcionModificada(){
        System.out.println("Introduce la nueva descripción:");
        return teclado.nextLine();
    }

    public boolean getModificarFecha(){
        System.out.println("¿Quiere modificar la fecha?(Si / No)");
        String decision = teclado.nextLine();
        return decision.equals("Si");
    }

    public String getFechaModificada(){
        System.out.println("Introduce la nueva fecha (dd-mm-yyyy hh-mm-ss)");
        return teclado.nextLine();
    }

    public int getCodigo() {
        System.out.println("Introduce el código de la tarea:");
        int codigo = teclado.nextInt();
        teclado.nextLine();
        return codigo;
    }

    public LocalDate getFecha() {
        System.out.println("Introduce la fecha de la tarea: (dd-mm-yyyy)");
        DateTimeFormatter dateTimeFormatter =
                DateTimeFormatter.ofPattern("dd-MM-yyyy");
        return LocalDate.parse(teclado.nextLine(), dateTimeFormatter);
    }

    public void showConfirmationMessage() {
        System.out.println("Tarea añadida con éxito");
    }

    private Prioridad getPrioridad(String prioridad){
        return switch (prioridad){
            case "Critica" -> Prioridad.CRITICA;
            case "Alta" -> Prioridad.ALTA;
            case "Normal" -> Prioridad.NORMAL;
            case "Baja" -> Prioridad.BAJA;
            default -> null;
        };
    }
}
