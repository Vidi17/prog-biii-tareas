package es.vidal.vista;

import com.github.freva.asciitable.AsciiTable;
import es.vidal.modelo.Tarea;

import java.time.format.DateTimeFormatter;
import java.util.ArrayList;

public class ListadoTareasView {

    private ArrayList<Tarea> tareas;

    public ListadoTareasView(ArrayList<Tarea> tareas) {
        this.tareas = tareas;
    }

    public void show() {
        DateTimeFormatter dateTimeFormatter =
                DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm:ss");
        String[] headers = {"codigo", "Prioridad","descripcion", "fecha creación",
                "fecha entrega", "estado"};
        String[][] data = new String[tareas.size()][6];
        for (int i = 0; i < data.length ; i++) {
            if (tareas.get(i) != null){
                data[i][0] = String.valueOf(tareas.get(i).getCodigo());
                data[i][1] = tareas.get(i).getPrioridad().toString();
                data[i][2] = tareas.get(i).getDescripcion();
                data[i][3] =
                        tareas.get(i).getFechaCreacion().format(dateTimeFormatter);
                data[i][4] =
                        tareas.get(i).getFechaEntrega().format(dateTimeFormatter);
                data[i][5] = tareas.get(i).isCerradaToString();
            }
        }
        System.out.println(AsciiTable.getTable(headers, data));
    }
}
