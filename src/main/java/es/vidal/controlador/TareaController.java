package es.vidal.controlador;

import es.vidal.modelo.Tarea;
import es.vidal.modelo.dao.TareaDaoInterface;
import es.vidal.vista.ListadoTareasView;
import es.vidal.vista.TareaInputView;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;

public class TareaController {

    public void listarTareasAction(TareaDaoInterface tareaDaoInterface) {
        //Llamamos al módulo para obtener el listado de tareas
        ArrayList<Tarea> listadoTareas = tareaDaoInterface.findAll();

        //Mostramos el listado a través de la vista
        ListadoTareasView listadoTareasView = new
                ListadoTareasView(listadoTareas);
        listadoTareasView.show();

    }

    public void anyadirTareaAction(TareaDaoInterface tareaDaoInterface) {
        // Preguntamos al usuario por los datos de la nueva tarea
        TareaInputView tareaInputView = new TareaInputView();
        Tarea tarea = tareaInputView.getNuevaTarea(tareaDaoInterface.findAll().size());
        // Guardamos la tarea en el capa de persistencia
        tareaDaoInterface.save(tarea);
        tareaInputView.showConfirmationMessage();
    }

    public void cerrarTareaAction(TareaDaoInterface tareaDaoInterface){
        TareaInputView tareaInputView = new TareaInputView();
        int codigo = tareaInputView.getCodigo();
        Tarea tarea = tareaDaoInterface.findById(codigo);
        if (tarea != null){
            tarea.setCerrada(true);
            tareaDaoInterface.save(tarea);
        }
    }

    public void modificarDescripcionFechaAction(TareaDaoInterface tareaDaoInterface){
        TareaInputView tareaInputView = new TareaInputView();
        int codigo = tareaInputView.getCodigo();
        Tarea tarea = tareaDaoInterface.findById(codigo);
        if (tarea != null){
            boolean modificarDescripcion = tareaInputView.getModificarDescripcion();
            if (modificarDescripcion){
                String descripcion = tareaInputView.getDescripcionModificada();
                tarea.setDescripcion(descripcion);
                tareaDaoInterface.save(tarea);
            }
            boolean modificarFecha = tareaInputView.getModificarFecha();
            if (modificarFecha){
                DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm:ss");
                LocalDateTime fecha = LocalDateTime.parse(tareaInputView.getFechaModificada(), dateTimeFormatter);
                tarea.setFechaEntrega(fecha);
                tareaDaoInterface.save(tarea);
            }
        }
    }

    public void cerrarTareasDiaAction(TareaDaoInterface tareaDaoInterface){
        TareaInputView tareaInputView = new TareaInputView();
        LocalDate fecha = tareaInputView.getFecha();
        ArrayList<Tarea> tareas = tareaDaoInterface.findByDate(fecha);
        for (Tarea tarea: tareas) {
            tarea.setCerrada(true);
            tareaDaoInterface.save(tarea);
        }
    } 
}
