package es.vidal.controlador;

import es.vidal.modelo.dao.TareaDaoInterface;
import es.vidal.vista.MenuView;

public class MenuController {

    private final TareaDaoInterface TAREA_DAO_INTERFACE;

    public MenuController(TareaDaoInterface tareaDaoInterface) {
        this.TAREA_DAO_INTERFACE = tareaDaoInterface;
    }

    public void mostrarMenu(){
        boolean finalizarMenu;
        do {
            finalizarMenu = seleccionarOpcionAction();
        }while (!finalizarMenu);
        System.out.println("Hasta pronto");
    }

    private boolean seleccionarOpcionAction(){
        MenuView menuView = new MenuView();
        int opcion = menuView.seleccionarOpcion();
        TareaController tareaController = new TareaController();

        boolean finalizarMenu = false;

        switch (opcion){
            case 1 -> tareaController.listarTareasAction(TAREA_DAO_INTERFACE);
            case 2 -> tareaController.anyadirTareaAction(TAREA_DAO_INTERFACE);
            case 3 -> tareaController.cerrarTareaAction(TAREA_DAO_INTERFACE);
            case 4 -> tareaController.cerrarTareasDiaAction(TAREA_DAO_INTERFACE);
            case 5 -> tareaController.modificarDescripcionFechaAction(TAREA_DAO_INTERFACE);
            default -> finalizarMenu = true;
        }
        return finalizarMenu;
    }
}
